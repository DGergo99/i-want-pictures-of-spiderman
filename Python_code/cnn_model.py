# -*- coding: utf-8 -*-
"""
Created on Sat Feb  6 00:58:45 2021

@author: gdvid

MODEL OF ABOUT 87% VALIDATION ACCURACY AS OF 2021.02.13, ABOUT 3000 TRAINING AND 1200 VALIDATION IMAGES OF SPIDERMAN
SCRAPED FROM DIFFERENT WEBSITES
"""

import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import pandas as pd
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, MaxPooling2D, Dropout, LeakyReLU
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.regularizers import l2 as l2
import random

# adding path for easier image access

path = 'D:/Python Projects/'

def load_lists(list1, list2, path_ending):
    #path contains multiple needed directories
    new_path = path + path_ending
    t_set = os.listdir(new_path)
    #looping through images
    for i in range(len(t_set)):
        # getting image
        img = mpimg.imread((new_path + '/' + t_set[i-1]))[:,:,0:3]
        # if image is float32, it means it has values between 0-1, most training images have 0-255 so we convert
        if isinstance(img[1][1][1], np.floating):
            img *= 255
            img = img.astype(np.uint8) # converting to uint8 type
        # adding the image to the list    
        list1.append(img)
        # the images are named in the dataset, so we can label them easily
        if t_set[i-1][0:9] == 'spiderman':
            list2.append('spiderman')
        else:
            list2.append('not_spiderman')
            
    return(list1, list2)

def create_arrays(path_ending):
    # 1 for image, 1 for label
    list1 = []
    list2 = []
    
    list1, list2 = load_lists(list1, list2, path_ending)
    # shuffles both lists the same
    indexes = list(zip(list1, list2))
    random.shuffle(indexes)
    list1, list2 = zip(*indexes)

    return list1, list2

def create_model():
    #creating the neural net
    cnn_model = Sequential()
    
    # conv1
    cnn_model.add(Conv2D(32, kernel_size=(5,5), padding = 'same', activation = LeakyReLU(alpha = 0.005),
                         input_shape=(128, 128, 3), use_bias = False, kernel_regularizer = l2( l = 0.01)))

    #pool1
    cnn_model.add(MaxPooling2D(pool_size=(2, 2)))
    
    # conv2
    cnn_model.add(Conv2D(32, kernel_size=(3,3), padding = 'valid', activation = LeakyReLU(alpha = 0.005), kernel_regularizer = l2( l = 0.01)))

    # pool2
    cnn_model.add(MaxPooling2D(pool_size=(2, 2)))
    
    # conv3
    cnn_model.add(Conv2D(64, kernel_size=(3,3), padding = 'valid', activation = LeakyReLU(alpha = 0.005), kernel_regularizer = l2( l = 0.01)))

    # pool3
    cnn_model.add(MaxPooling2D(pool_size=(2, 2)))
    
    # conv4
    cnn_model.add(Conv2D(128, kernel_size=(3,3), padding = 'valid', activation = LeakyReLU(alpha = 0.0050), kernel_regularizer = l2( l = 0.01)))

    # pool4
    cnn_model.add(MaxPooling2D(pool_size=(2, 2)))
    
    # conv5
    cnn_model.add(Conv2D(256, kernel_size=(3,3), padding = 'valid', activation = LeakyReLU(alpha = 0.0050), kernel_regularizer = l2( l = 0.01)))

    # pool5
    cnn_model.add(MaxPooling2D(pool_size=(2, 2)))
    
    # flatten
    cnn_model.add(Flatten())
    
    # dense
    cnn_model.add(Dense(124, use_bias = True, kernel_regularizer = l2( l = 0.01), activation = LeakyReLU(alpha = 0.005)))
    cnn_model.add(Dropout(0.2))

    # out
    cnn_model.add(Dense(1, activation='sigmoid'))
    
    #return
    return cnn_model

# images are in 'True_T...' directories

training_images, training_labels = create_arrays('True_TrainS_')
testing_images, testing_labels = create_arrays('True_TestS_')

# from tuple to np array

training_images = np.asarray(training_images)
testing_images = np.asarray(testing_images)

# either spiderman or not spiderman, so we only need one one-hot vector

dummy_train_label = pd.get_dummies(training_labels).spiderman
dummy_test_label = pd.get_dummies(testing_labels).spiderman

# creating the network

full_model = create_model()

# compiling

full_model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# with flipped images: (https://learnopencv.com/image-classification-using-convolutional-neural-networks-in-keras/)

datagen = ImageDataGenerator(
        width_shift_range = 0.4,
        height_shift_range = 0.4,
        zoom_range = 0.4,
        horizontal_flip = True,
        vertical_flip = True,
        rotation_range = 40,
        rescale = None)

# training and saving history to print curves

history2 = full_model.fit(datagen.flow(x = training_images, y = dummy_train_label, 
                         batch_size = 16), epochs = 10, batch_size = 64,
                         validation_data=(testing_images, dummy_test_label))


"""
first100 = full_model.predict(testing_images[500:600])
plt.imshow(testing_images[558])

for i in range(len(testing_images)):
    print("i: " + str(i))
    print("modell: " + str(first100[i]))
    print("real: " + str(dummy_test_label[i+500]))
    print("-----------------------------------")
"""

# loss

plt.figure(figsize=[8,6])
plt.plot(history2.history['loss'],'r',linewidth=3.0)
plt.plot(history2.history['val_loss'],'b',linewidth=3.0)
plt.legend(['Training loss', 'Validation Loss'],fontsize=18)
plt.xlabel('Epochs ',fontsize=16)
plt.ylabel('Loss',fontsize=16)
plt.title('Loss Curves',fontsize=16)
 
# accuracy

plt.figure(figsize=[8,6])
plt.plot(history2.history['accuracy'],'r',linewidth=3.0)
plt.plot(history2.history['val_accuracy'],'b',linewidth=3.0)
plt.legend(['Training Accuracy', 'Validation Accuracy'],fontsize=18)
plt.xlabel('Epochs ',fontsize=16)
plt.ylabel('Accuracy',fontsize=16)
plt.title('Accuracy Curves',fontsize=16)

# saving the model

full_model.save(filepath = (path + 'I want pictures of Spiderman/Models/20210213_87_acc.h5'), save_format = 'h5')











