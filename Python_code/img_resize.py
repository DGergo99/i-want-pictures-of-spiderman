# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 21:43:27 2021

@author: gdvid
"""

from PIL import Image
import os, sys

# setting up path

path = "to_resize/"

# listing the elements of path

dirs = os.listdir(path)

# for every item (if exists) we resize it to 128x128

for item in dirs:
    if os.path.isfile(path+item):
            im = Image.open(path+item)
            f, e = os.path.splitext(path+item)
            imResize = im.resize((128,128), Image.ANTIALIAS)
            imResize.save(f + '.jpg', 'PNG', quality=90)
    else:
            print('error')

