# -*- coding: utf-8 -*-
"""
Created on Thu Dec 24 14:51:24 2020

@author: gdvid
"""

# using selenium for scraping

from selenium import webdriver
import requests

# links of spiderman images from wiki and alamy (alamy xpath is different)

links = [""]

# Firefox webdriver (having to set up geckodriver)

driver = webdriver.Firefox()
i = 0

# for every link we open a driver and collect elements with the given css class

for j in links:
    driver.get(j)
    imgsrc = driver.find_elements_by_xpath("//img[contains(@class,'category-page__member-thumbnail')]")
    for ele in imgsrc:
      # saving every image to a directory with i as an index
      x = ele.get_attribute("src")
      img = requests.get(x)
      i = i+1
      path = 'images\spiderman' + str(i) + '.jpg'
      with open(path, 'wb') as writer:
          writer.write(img.content)
          