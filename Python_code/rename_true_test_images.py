# -*- coding: utf-8 -*-
"""
Created on Sat Feb  6 17:38:12 2021

@author: gdvid
"""

import os 

# renaming elements collected from different sources

a = os.listdir("Spiderman_in_train/")

i = 1

for element in a:
    os.rename(("Spiderman_in_train/" + element), ("Spiderman_in_train/spiderman_train" + str(i) + ".jpg"))
    i = i+1
    